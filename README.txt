Corpus épigraphique bourguignon (VIIIe-XVe siècle)

Projet réalisé en 2018 par les équipes du CIFM (Corpus des Inscriptions de la France Médiévale  - CESCM-Poitiers) et CBMA (Corpus Burgundiae Medii Aevi  - LaMOP - Paris). 
Il est porté par le LaMOP (Laboratoire de Médiévistique Occidentale de Paris - UMR 8589) et le CESCM (Centre d’études supérieures de civilisation médiévale - UMR 7302). Il a bénéficié en 2018 du soutien du LabEx haStec (en partenariat avec l’IRHT - Institut de recherche et d’histoire des textes – CNRS UPR 841, le Centre Jean Mabillon -EnC - EA 3624 et le LEM/CERCOR (Laboratoire d’Études sur les Monothéismes – UMR 8584 / Centre européen de recherche sur les congrégations et les ordres religieux) et du Consortium COSME2 (CNRS - TGIR Huma-Num).

Direction scientifique d’Eliana Magnani (CNRS-LaMOP) et Estelle Ingrand-Varenne (CNRS-CESCM).
Révision des inscriptions publiées et addition des inédites par Aurore Menudier (CESCM).
Développements informatiques et adaptation de Philologic4 par Pierre Brochard (CNRS-LaMOP).
Localisation dans les diocèses (SIG) par Davide Gherdevich (Université Versailles-Saint-Quentin - DYPAC).
Lemmatisation par Nicolas Perreaux (Université de Francfort).
Webmestre : Coraline Rey (Université de Bourgogne), Mathieu Beaud (Université Paris 1 - LaMOP).

Le corpus

Ce corpus n’est pas exhaustif. 
Il contient 1418 inscriptions épigraphiques provenant de cinq départements français contemporains — Jura, Nièvre, Saône-et-Loire, Côte-d’Or et Yonne.
    • 471 inscriptions sont publiées dans les volumes 19 (1997), 20 (1999) et 21 (2000) du CIFM. Elles ont été corrigées et mises à jour quand nécessaire.  
    • 947 inscriptions étaient inédites, mais compilées dans le fichier papier par l’équipe du CIFM.

Le corpus est diffusé sous les formats suivants : 
    • tableau .csv (métadonnées et textes) (https://gitlab.huma-num.fr/lamop/cbma-epigraphie/tree/master)
    • fichiers .txt (unités textuelles regroupées par département et par langue)(https://gitlab.huma-num.fr/lamop/cbma-epigraphie/tree/master)
    • fichiers .xml (textes lammatisés pour TXM) (en cours)
    • interrogation sous Philologic4 (http://philologic.lamop.fr/epigraphie/)
    • base de données sous FileMakePro (corpus incorporé à la base des CBMA)

Les métadonnées retenues sont :
    • ID projet
    • Dpt (département)	
    • Ville	
    • id wikidata	
    • Latitude (WGS-84)	
    • Longitude (WGS-84)
    • Localisation	
    • Titre	
    • Date (Date simplifiée à l’année, voir les critères adoptés ci-dessous) 	
    • Date CIFM	
    • Texte développé	
    • Langue 
    • français
    • latin
    • latin_fr  (latin et français, où le latin prédomine)
    • fr_latin (français et latin, où le français prédomine)
    • grec
    • Corrections/commentaires	
    • nomfichiertext	
    • Auteur de la notice	
    • URL (vers volumes du CIFM)
    • identifiant TOPAMA. La base TOPAMA décrit les évêchés et archevêchés du Moyen-Age, le projet est coordonnée par Thomas Lienhard.
    • Diocèse

Date simplifiée (de 750 à 1508). Critères adoptés :
    • Préférence au nouveau style
    • Date inconnue = 9999
    • Circa 930 = 0930
    • Après 930 = 0930
    • Avant 930 = 0930
    • Dans les tranches chronologiques, le terminus post quem a été retenu : 1063-1084 = 1063
    • Dans la datation de l’éditeur, l’année exprimée en chiffres (la plus ancienne, en cas de plusieurs années exprimées) a été retenue : Fin du XIe siècle, ou XIIe avant 1124 = 1124 ; 1111, 1115 ou 1118 = 1111
    • Pour un siècle, sans précision, la cinquantaine a été retenue : XIe siècle = 1050 ; XIIe siècle = 1150, etc.
    • Pour le milieu d’un siècle, la cinquantaine a été retenue : milieu du Xe siècle = 0950
    • Début, premier quart, premier tiers, première moitié d’un siècle, la centaine a été retenue : début du XIIe siècle = 1100 ; premier quart du XIe siècle = 1000 etc.
    • Fin, deuxième moitié d’un siècle, l’expression 99 a été retenue : Fin du XIe siècle = 1099 ; deuxième moitié du XIIe siècle = 1199
    • La date la plus ancienne prévaut : Fin du XIe siècle ou premier quart du XIIe = 1099

--------------------------------------------------------------------------------

Bibliographie complémentaire. Côte d’Or (21), Jura (39), Nièvre (58), Saône-et-Loire (71) et Yonne (89) 
 
Cette bibliographie complète les bibliographies générales des volumes 19 (p. v-x), 20 (p. vii-ix) et 21 (p. 7-10) du CIFM. Elle n’est pas exhaustive et comportera des mises-à-jour. Pour signaler des rectifications et des compléments, veuillez écrire à cbma.project@gmail.com
Les références contiennent une ou plusieurs inscriptions éditées. La majorité, si ce n’est la totalité, des inscriptions sur un site sont répertoriées dans une même référence. Certaines sont augmentées de précisions entre parenthères ( ) pour retrouver plus facilement l’inscription concernée, ou l’ensemble, grâce au nom du site, au nom du défunt ou tout autre élément indicateur.

Favreau R.,  Michaud J., Mora B., Jura, Nièvre, Saône-et-Loire, Paris, 1997 (Corpus des inscriptions de la France médiévale, 19) - http://www.persee.fr/doc/cifm_0000-0000_1997_cat_19_1 
Favreau R.,  Michaud J., Mora B., Côte-d’Or, Paris, 1999 (Corpus des inscriptions de la France médiévale, 20) - http://www.persee.fr/doc/cifm_0000-0000_1999_cat_20_1 
Favreau R., Michaud J., Yonne, Paris, 2000 (Corpus des inscriptions de la France médiévale, 21) - http://www.persee.fr/doc/cifm_0000-0000_2000_cat_21_1
 
Côte d’Or (21)
Sources et manuscrits
Dijon, Archives départementales de la Côte d’Or, Fonds C.A.C.O 69 J 74 II (Saint-Bénigne de Dijon).
Dijon, Archives départementales de la Côte d’Or, Titre de l’Hôpital aux Riches (publié incomplètement par M. d’Arbaumont dans sa notice historique sur la chapelle et l’hôpital aux Riches, Mémoires de la Commission des Antiquités de Côte-d’Or, t. VII, p. 90).
Dijon, Bibliothèque municipale, ms. 1972, Lachère, Nécrologe du couvent des frères mineurs.
Paris, BnF, coll. Bourgogne, tome 14.
Paris, BnF, ms. fr. 8226, Recueil d’épitaphes des églises de Bourgogne, duché et comté, Autun et Dijon, Chalons, Nivernois, Lion et des environs, vol. II.
Gallia christiana, t. VIII, Paris, 1744.
Bibliographie
Abord-Belin, « Santenay en 1852 et ses antiquités », Congrès archéologique, Sens, 1852 (p. 334).
André L., L’abbaye de Fontenay : de saint Bernard au patrimoine mondial, Belin-Herscher, Paris, 2002.
Aubert M., La Bourgogne. La sculpture, Paris, 1930, t. III.
Baron F., « Le médecin, le prince, les prélats et la mort. L’apparition du transi dans la sculpture française au Moyen Âge », Cahiers archéologiques, 2003-2004, n° 51, p. 125-158 (p. 136 : Dijon, Jacques Germain).
Baudot H., « Rapport sur la découverte des peintures murales de l’église de Bagnot (Côte d’Or), Mémoires de la Commission des Antiquités de Côte d’Or, 1861-64, t. VI, p. 205-212.
Bégule L., L’abbaye de Fontenay et l’architecture cistercienne, Paris, 1950.
Bougaud L. E. (abbé), Étude historique et critique sur la mission, les actes et le culte de Saint Bénigne…, Paris, 1859.
Bresson J., Histoire de l’église Notre-Dame de Dijon, Dijon, 1891.
Castellane (marquis de), Inscriptions du Ve au XVIe siècle…, Toulouse, 1838 (p. 319 : Jean Roussin, Beaune ; p. 191 : cloche Notre-Dame de Dijon).
Catalogue du Musée de la Commission des Antiquités du département de la Côte d’Or, Dijon, 1894.
Chabeuf H., « Jean de la Huerta, Antoine le Moiturier et le tombeau de Jean sans Peur », Mémoires de l’Académie des sciences, arts et belles-lettres Dijon, 1890-91, 4e série, t. II (p. 144 : Claus de Werve).
Chabeuf H., « Les tapisseries de l’église Notre-Dame de Beaune », Mémoires de la Commission des Antiquités de Côte d’Or, 1895, t. XII, p. 213-223.
Chabeuf H., « L’église Saint-Bénigne de Dijon », Revue de l’Art chrétien, 1895, 5e série, t. IV, p. 392-402, 469-474.
Chabeuf H., « Les tapisseries de l’église Notre-Dame de Beaune », Revue de l’Art chrétien, 1900, 5e série, t. XI, p. 193-205.
Chabeuf H., « Séance du 2 mai 1907. Compte rendu des travaux de la Commission des Antiquités », Mémoires de la Commission des Antiquités de Côte d’Or, 1907-1908, t. XV (église Saint-Jean, Dijon).
Chabeuf H., « Compte rendu des travaux de la Commission des Antiquités », Mémoires de la Commission des Antiquités de Côte d’Or, t. XVII (Fontaine-lès-Dijon).
Chabeuf H., « La Sainte-Chapelle de Dijon », Revue de l’Art chrétien, 1911, t. LXI, p. 190 (Thomas de Saulx).
Chabeuf H., « Histoire d’une église, 2e partie. Epigraphie de l’église de Saint-Seine », Mémoires de la Commission des Antiquités de Côte d’Or, t. XI, 1885-88.
Chatelet A. « Résurrection de Pierre Coustain », Bulletin de la Société de l’Histoire de l’Art français, 1963 (paru 1936) (saint Jacques, Dijon).
Chomtom L. (abbé), Histoire de l’église Saint-Bénigne de Dijon, Dijon, 1900.
« Compte-rendu des travaux de la Commission des antiquités », Mémoires de la Commission des Antiquités de Côte d’Or, 1847-52, t. III (abbaye de Fontenay) ; 1853-56, t. IV (Semur-en-Auxois) ; 1885-1888, t. XI (Ruffey-les-Echirey) ; 1890-91, t. XII (Epagny) ; 1901, t. XIV (Ruffy-lès-Beaune) ; 1916-18, t. XVII (p. CCLVI : Bezouotte).
Cornereau, « Epigraphie bourguignonne, Les Hôpitaux du Saint-Esprit et de Notre-Dame de la Charité », Mémoires de la Commission des Antiquités de Côte d’Or, 1895, t. II.
Dedieu (r. p. Hugues), « Les sépultures de quelques églises franciscaines du nord de la Loire d’après les dessins de la collection Gaignières. Répertoire géographico-chronolgique », Archivum franciscanum historicum, 1978, t. LXXI, n°1-2 (p. 16 : Châtillon-sur-Seine).
De Vaivre J.-B., « Dessins de tombes médiévales bourguignonnes de la collection Gaignières », Gazette des Beaux-arts, nov. 1986.
Dumay G., « Carreaux émaillés recueillis en Bourgogne », Bulletin de la Société des Antiquaires de France, 1877 (l’Etang-Vergy).
Dumay G., « Eglise et abbaye de Saint-Bénigne de Dijon », Mémoires de la Commission des Antiquités de Côte d’Or, Paris-Dijon, 1882.
Dumay G., « Les tombes de l’église de Rouvres », Mémoires de la Commission des Antiquités de Côte d’Or, 1895, t. XII.
Enlart C., « La sculpture des portails de la cathédrale d’Auxerre du XIIIe à la fin du XIVe siècle », Congrès archéologique de France, Avallon, 1907, Paris-Caen, 1908 (p. 615 : Semur-en-Auxois).
Fabrege F., Histoire de Magueline, Paris-Montpellier, 1911, t. III (p. 388, hôpital du Saint-Esprit).
Flouest, « Pierres tombales du canton de Recey-sur-Ource », Mémoires de la Société des Antiquaires de France, 1883, 5e série, t. IV (p. 226 : Bure-les-Templiers).
Foisset P., « Restes du château féodal de Meursault », Mémoires de la Commission des Antiquités de Côte d’Or, 1865-69, t. VII, p. 254.
Fontenay H. de, Epigraphie autunoise. Inscriptions du moyen âge et des temps modernes pour servir à l’histoire d’Autun, Autun-Paris, 1883, t. II.
Fontenay J. de, « 1ère séance du 4 juillet », Congrès archéologique de France. Séances générales tenues à Dijon en 1852 par la Société française pour la Conservation des Monuments historiques, Paris, 1853 (Beaune).
Frizon P., Gallia purpurata, Paris, 1638 (p. 259 : abbaye de Cîteaux).
Gaignières (coll.), Tombeaux et épitaphes des églises de France - Paris, BnF, Département des Estampes et de la photographie, RESERVE PE-11C-FOL.
Garnier J., « Notice historique de la Maladière de Dijon », Mémoires de l’Académie des sciences, arts et belles-lettres, Dijon, 1852-53, 2e série, t. II.
Gauthier J., « Tombes franc-comtoises inédites des XIVe-XVIIIe siècles », Académie des Sciences, belles-lettres et arts Besançon, 1885.
Gonse L., Les chefs-d’œuvre des Musées de France. Sculpture. Dessins. Objets d’Art, Paris, 1904 (p. 152 : Dijon).
Greenhill F. A., Incised Effigial Slab. A Study of Engraved Stone Memorials in Latin Christendom, c. 1100 to c. 1700, t. I-II, Londres, 1976.
Grillon G., L’ultime message : étude des monuments funéraires médiévaux de la Bourgogne ducale (XIIe-XVIe siècle), thèse d’histoire médiévale, Université de Bourgogne, 2011 (dir. Vincent Tabbagh).
Grillon G., « Les plates-tombes bourguignonnes : la constitution d’un modèle (XIIe-XIIIe siècles) », Les Cahiers de Saint-Michel-de-Cuxa, XLII, 2011, p. 215-220.
Guilhermy F. de, « Communications diverses ; découvertes de sépultures ; inscriptions Revue de la Société des sav. des départements, 1877, 6e série, VI.
Hurot (abbé), « Notice sur l’église de Rouvres », Congrès archéologique de France. Séances générales tenues à Dijon en 1852 par la Société française pour la Conservation générale des Monuments historiques, Paris, 1853.
Jannet M., Penichon D., « Les pierres tombales du prieuré de Bonvaux », Bulletin des Musées de Dijon, fasc. 1, Dijon, 1995.
Klein Clausz A., Dijon et Beaune, Paris, 1913.
« Les restes des ducs et princesses de Bourgogne. Appendice. Note sur l’année du décès d’Isabelle de Portugal », Mémoires de la Commission des Antiquités de Côte d’Or, 1905, t. XIV (p. 232 : Isabelle de Portugal).
Levert R., La collection des pierres tombales du musée archéologique de Dijon, Mémoire de maîtrise, Université de Bourgogne, dir. D. Russo, Dijon, 2000.
Marilier J. (chan.), « Les tombes médiévales de l’église d’Arceau », Mémoires de la Commission des Antiquités de Côte d’Or, 1984-86, t. XXXIV.
Marcoux R., « La terre, la famille, le ciel ; les sépultures de la maison de Saulx aux XIIIe et XIVe siècles », Inhumations de prestige ou prestige de l’inhumation ? Expressions du pouvoir dans l’au-delà, IVe-XVe siècle, éd. A. Arduc-le-Bagousse, Caen, 2009, p. 329-355.
Marion J., « Notice sur l’abbaye de la Bussière », Bibliothèque de l’Ecole des Chartes, 1842-1843, 1ère série, t. IV.
Marion J. « Etudes archéologique sur les églises du département de la Côte d’Or », Bulletin monumental, 1844, t. X (p. 157 : Rouvres).
Marsy (comte de), « Le musée des antiquités de la Côte d’Or », Bulletin monumental, 1896, t. LXI.
Martin J., Jeanton G., « Les pierres tombales circulaires et ovales de la Bourgogne (quinzième au dix-septième siècle), Réunion Soc. Beaux-arts, départ., 34e session, 1910 (Saint-Pierre de Dijon, p. 29).
Metman, « Séance du 1er février 1908. Compte rendu des travaux de la Commission des Antiquités », Mémoires de la Commission des Antiquités de Côte d’Or, 1908-09, t. XV.
Mignard, « Histoire des principales fondations religieuses du baillage de la Montagne. Abbaye de Saint-Seine », Mémoires de la Commission des Antiquités de Côte d’Or, p. 245.
Millin A.-L., Voyage dans les départements du Midi de la France, Paris, 1807, t. I (p. 196 : Semur-en-Auxois).
Mirimonde A. P. de, « Les sujets de musique dans les œuvres allemandes au Musée du Louvre », Revue du Louvre, 1964, t. XIII (Dijon, p. 119, retable).
Morillot L. (abbé), Etude sur l’emploi des clochettes… (clochette de l’Angelus, Dijon).
Morillot L. (abbé), « L’ancienne église de Saint-Julien-en-Val », Bulletin d’histoire et d’archéologie religieuses du diocèse de Dijon, janv-fév. 1883.
Norton Ch., « Les carreaux de pavage de la Bourgogne médiévale », Archéologia, avril 1982, n° 165.
« Notice sur le beffroi municipal et son horloge », Mémoires de la Société archéologique de Beaune (Côte d’Or), Histoire Lettres Sciences et Arts, 1908-1909 (p. 57, anc. beffroi Beaune).
Oursel Ch., L’église Notre-Dame de Dijon, Paris, 1941 (Petite monographie des grands édifices de la France).
Petit E., Histoire des ducs de Bourgogne de la race capétienne, Darantière, Dijon, 1885-1895, t. VIII.
Petit V., « Rapport sur la visite des monuments de Beaune », Congrès archéologique de France. Séances générales tenues à Dijon en 1852 par la Société française pour la Conservation des Monuments historiques, Paris, 1853 (Beaune).
Plancher U. (dom), Histoire généalogique et particulière de Bourgogne, Antoine du Fay, Dijon, 1739-1781, t. II.
Quarré P., « Musées de province. L’exposition du Trésor de Bourgogne, à Dijon », Musées de France, 1948 (août-sept).
Rauwel A., « Pour un corpus des inscriptions de Côte-d’Or antérieures à 1300 », Mémoires de la Commission des Antiquités de Côte d’Or, t. 39, p. 59-73 (les corrections indiquées comme venant d’A. Rauwel sont extraites de cet article).
« Recueil des inscriptions des principales églises de Besançon d’après les manuscrits de Jules Chifflet et divers documents ou monuments », Académie des Sciences, Belles-lettres et Arts de Besançon, 1881 (p. 196 : Pagny-le-Château).
Rhein A., « Beaune », Congrès archéologique de France, Dijon, 1928 (Paris, 1929).
Rivières (baron de), « Inscriptions et devises horaires », Bulletin monumental, t. XLIV, 1878 (cloche Notre-Dame de Dijon).
Rossignol M., « Lettre sur la devise du chancelier Rolin et sur ses pavés émaillés », Mémoires de la Commission des Antiquités de Côte d’Or, 1853-56, IV.
Sagot E., « Lettres ou inscriptions sur les vêtements des statues », Bulletin archéologique, 1847-48, 4e vol. (p. 520 : Chartreuse de Champmol).
Sauvageot C., « Etude sur les cloches », Annales archéologiques, 1862, t. XXII (p. 234-235 : Santenay).
« Séance du 15 novembre 1907 », Compte rendu des travaux de la Commission des Antiquités, Mémoires de la Commission des Antiquités de Côte d’Or, 1908-09, t. XV (p. CXXX-CXXXI : Dijon).
Soultrait G. de, « Tombes de la léproserie de Dijon », Bulletin du comité de la Langue, de l’histoire et des arts de la France, 1855-56, t. III.
Tardy, Les ivoires. Evolution décorative du Ier siècle à nos jours, Paris, 1966 (p. 38 : Dijon).
Toutain J., « Sur une inscription aujourd’hui disparue de l’église Notre-Dame de Semur », Bulletin archéologique, 1946-49.
Vifs nous sommes…morts nous serons. La rencontre des trois morts et des trois vifs dans la peinture murale en France, Vendôme, 2001 (p. 77-78 : Boux-sous-Salmaise).
Vregille B. de, « Gautier II », Dictionnaire d’Histoire et de Géographie Ecclésiastiques, t. XX, Paris, 1984 (col. 77-78 : chartreuse de Lugny).
Jura (39)
Sources et manuscrits
Bibliographie
Berthele J., Enquêtes campanaires. Notes, études et documents sur les cloches et les fondeurs de cloches du VIIIe au XXe siècle, Montpellier, 1903.
Brune (abbé), « Notice sur trois cloches anciennes dans le Jura », Mémoires de la Société nationale des Antiquaires de France, 1889.
Brune (abbé), « Le mobilier et les oeuvres d’art de l’Eglise de Baume-les-Messieurs », Bull. Com. Tr. Hist. Arch., 1894.
Brune (abbé), « Notes iconographiques », Bull. Com. Tr. Hist. Arch., 1900.
Brune (abbé) et Gauthier J., « L’orfèvrerie en Franche-Comté, du VIIe au XVIIIe s. », Bull. Com. Tr. Hist. Arch., 1900.
Brune (abbé), « L’église de la Chaux-des-Crotenay (Jura) et ses oeuvres d’art », Réunion des Sociétés des Beaux-Arts des départements, 1914.
Comoy A., « Les stalles de Saint-Claude », Bulletin archéologique, 1842-43.
Duhem G., « La découverte de la pierre tombale de Jeanne de Bourbon en l’église des Cordeliers à Lons-le-Saunier », Revue d’émulation du Jura, janvier 1951.
Gauthier J., « Les inscriptions des abbayes cisterciennes du diocèse de Besançon », Académie des Sciences, Belles-Lettres et Arts Besançon, 1882.
Gauthier J., « Tombes Franc-comtoises inédites des XIVe-XVIIIe s. », Académie des Sciences, Belles-Lettres et Arts Besançon, 1885.
Gauthier J., « Le Couvent des Cordeliers de Salins, son église et ses monuments », Bull. Com. Tr. Hist. Arch., 1896.
Gauthier J. « Dalles historiées, monuments et inscriptions funéraires recueillis avant 1790 dans les églises franc-comtoises », Académie des Sciences, Belles-Lettres et Arts Besançon, 1901.
Figuration (La) des morts dans la chrétienté médiévale jusqu’à la fin du premier quart du XIVe siècle, Fontevraud, 1989 (Cahiers de Fontefraud, 1).
Lauriere J. de, « Excursion de la Société française d’archéologie en Franche-Comté », Congrès archéologique France, Arras, 1880, Caen-Paris, 1881, p. 513-561.
Lauriere J. de, « La Société française d’archéologie en Franche-Comté », Bulletin Monumental, 1881.
Marquiset A., Statistique historique de l’arrondissement de Dôle, Besançon, 1841 (t. 1), 1842 (t. 2).
Melcot, Le Jura. Dictionnaire historique géographie et statistique du département, Lons-le Saunier, 1885.
Monnier F., « Annuaire du département du Jura », Annales Anciennes, 1852.
Montaiglon A., « Compte rendu des mémoires de la société d’émulation du Jura, année 1877 », Revue des Sociétés savantes des départements, 1882.
Prinet M., « Inscription du reliquaire de la sainte Epine de Poligny », Bulletin de la Société des Antiquaires de France, 1917.
Prost B., « Notice sur sept dalles funéraires franc-comtoises », Mémoires de la Société d’émulation du Jura, 1875, 1878.
« Recueil des inscriptions des principales églises de Besançon d’après les manuscrits de Jules Chifflet et divers documents ou monuments », Académie des Sciences, Belles-Lettres et Arts Besançon, 1881.
Nièvre (58)
Sources et manuscrits
Bibliographie
Anfray M., L’architecture religieuse du Nivernais au moyen âge. Les églises romanes, Paris, 1951.
Anfray M., La cathédrale de Nevers et les églises gothiques du Nivernais, Paris, 1964.
Aubertin M. C., « Notice sur la sépulture de Guigone de Salins, veuve de Nicolas Rolin, chancelier de Bourgogne, fondateur du grand Hôtel Dieu de Beaune », Bulletin d’histoire et d’archéologie religieuses du diocèse de Dijon, 1888.
Crosnier A. J., « Visite à la cathédrale de Nevers », Congrès archéologique de France, Laon, Nevers, Gisors, 1851, Paris, 1852, p. 314-327.
Crosnier A. J., Soultrait G. de, Séance du 13 juin, Congrès archéologique de France, Laon, Nevers, Gisors, 1851, Paris, 1852, p. 250-304.
Gauthier G., « La pierre tombale d’Henri de Saxe », Bulletin de la Société nivernaise des Lettres, Sciences et Arts, 1908.
Morellet N.-J., Barat J.-C. et Bussiere E., Le Nivernois, album historique et pittoresque, Grenoble, 1969 (Nevers, 1838-1840).
Serbat L., « Nevers », Congrès archéologique de France, Moulins, Nevers, 1913, Paris-Caen, 1916, p. 300-373.
Soultrait G. de, « Épigraphie de l’arrondissement de Nevers », Bulletin des Commissions Hist. Arch. Beaux-Arts, 1852.
Soultrait G. de, « Statistique monumentale de l’arrondissement de Nevers », Bulletin Monumental, 1851.
Soultrait G. de, Répertoire archéologique du département de la Nièvre, Paris, 1875 (Répertoire archéologique de la France, dir. Comité des Travaux Historiques et des Sociétés savantes).
Tiersonnier P., « Souvenirs bourbonnais à Saint-Pierre-le-Moutier », Bulletin de la Société d’émulation du Bourbonnais, 1929.
Saône-et-Loire (71)
Sources et manuscrits
Bibliographie
Armand-Calliat L., Catalogue des collections lapidaires du Musée de Chalon, Chalon-sur-saône, 1936.
Batault H., « Notice historique sur l’abbaye et les bénédictines de Lancharre et le prieuré du Penlay », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1857.
Bazin J.-L., « Notice historique sur l’abbaye de La Ferté-sur-Grosne », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1895.
Beaufils P., « Étude sur les effigies des pierres tombales », Réunion des sociétés des beaux-arts des départements, 33e session, 1909.
Bertrand E., « Séance du 21 janvier 1918 », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1920, p. 25-30.
Bertrand A., « Compte-rendu des Mémoires de la Société Éduenne », Revue des Sociétés savantes des départements, 1876.
Blanchet A., « Enseignes de pèlerinage », Bulletin de la Société des Antiquaires de France, 1932.
Bonnerot J., Autun et le Morvan, Paris, 1933.
Deschamps P. et Thibout M., La peinture murale en France au début de l’époque gothique. De Philippe Auguste à la fin du règne de Charles V (1180-1380), Paris, 1963.
Dickson M. et C., Les églises romanes de l’ancien diocèse de Chalon. Cluny et sa région, Mâcon, 1935.
Gagnard, P., Histoire de l’Église d’Autun, Autun, 1774.
Dumay G., « Les tombes de l’église de Rouvres », Mémoire de la Commission des antiquités du département de la Côte-d’Or, 1895.
Fontenay H. de, Épigraphie autunoise, Inscriptions du moyen âge et des temps modernes pouvant servir à l’histoire d’Autun, Autun-Paris, 1886.
Gauthier J., « Tombes Franc-comtoises inédites des XIVe-XVIIIe s. », Académie des Sciences, Belles-Lettres et Arts Besançon, 1885.
Martin J., « Pierres tombales de l’église de l’abbaye de Tournus », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1895.
Martin J. « L’église cathédrale Saint-Vincent de Chalon-sur-Saône, pierres tombales et documents historiques », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1906.
Martin J., « Église de l’ancienne abbaye du Miroir », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1920.
Martin J. et Jeanton G., « Pierres tombales figurées du département de Saône-et-Loire », Réunion des Sociétés des Beaux Arts des Départements, 1909.
Martin J. et Jeanton G., « Les pierres tombales circulaires et ovales de la Bourgogne (quinzième au dix-septième siècle) », Réunion des sociétés des beaux-arts des départements, t. 34, 1910, p. 79-94.
Mémoire de la Société d’histoire et d’archéologie de Chalon, 1926-27.
Niepce L., « Notice sur l’ancien Hôtel de Ville de Chalon-sur-Saône », Mémoire de la Commission des antiquités du département de la Côte-d’Or, 1857.
Nouvelle histoire de l’abbaïe royale et collégiale de Saint Filibert et de la ville de Tournus…, par un chanoine de la même abbaïe, Dijon, 1733.
Penjon A., Cluny. La ville et l’abbaye, Cluny, 1884.
Pequegnot F., « Notice historique sur le village de Rully », Mémoires de la Société d’histoire et d’archéologie de Chalon-sur-Saône, 1847/49.
Rivieres (baron de), « Inscriptions et devises horaires », Bulletin Monumental, 1878.
Sauvageot C., « Étude sur les cloches », Annales archéologiques, 1862.
Terret V. La sculpture bourguignonne aux XIIe et XIIIe siècles. Ses origines et ses sources d’inspiration, Autun, 1925.
Vaivre J.-B. de, « La dalle tumulaire de Jean de Bourbon seigneur de Montperroux », Bulletin Monumental, 1978.
Virey J., Les églises romanes de l’ancien diocèse de Mâcon. Cluny et sa région, Mâcon, 1935.
Yonne (89)
Sources et manuscrits
Auxerre, Bibliothèque Municipale, Ms. 144 (Catalogue auxerrois, XVIIe s.) (fol. 97 « Cimetière auxerrois », fol. 108 v., plusieurs inscriptions XVe siècle).
Auxerre, Bibliothèque Municipale, Ms. 157, fol. 1433, 1435.
Paris, BnF, Ms. nouv. acq. lat. 2057, Epitaphia et inscriptiones veteres (p. 306, épitaphe de Marie Seignelay).
Gallia christiana, t. IV, Paris, 1728 ; t. XII, Paris, 1759.
Gesta Pontificum Autissiodorensium (éd. Les gestes des évêques d'Auxerre, 3 tomes, sous la direction de M. Sot, Les Belles Lettres, Paris, 2002-2009)
Bibliographie
 
Les vitraux de Bourgogne, Franche-Comté et Rhône-Alpes, Paris, 1986 (Corpus Vitrearum, France, III).
Adhémar J., Dordor G., « Les tombeaux de la collection Gaignières. Dessins d’archéologie du XVIIe siècle », t. I, Gazette des Beaux-arts, 116, 1974.
Ame E., « Peintures de l’église de Savigny-en-Terre-Pleine et découverte de tombeaux dans la même église », Bull. des Com. Hist. Archéol. Beaux-arts, 1852, t. III, p. 175.
Barbier de Montault X., « Le parement d’autel de l’évêché de Montauban », Bulletin de la Société archéologique Tarn-et-Garonne, 1893, 21, p. 102 (Sens, parement d’autel, cathédrale Saint-Etienne).
Bastard L. de, « Note sur une pierre tumulaire d’un seigneur et d’une dame de Maligny », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 12, 1858 (Seigneley).
Bégule L., La cathédrale de Sens, son architecture, son décor, Lyon, 1929.
Bouvier (abbé H.), Histoire de l’abbaye Saint-Pierre-le-Vif, Auxerre, 1891.
« Catalogue raisonné du Musée d’Auxerre », Bulletin de la Société des fouilles archéologiques et des monuments historiques de l’Yonne, 37, 1883.
Chale M. A., « Histoire de la ville et du comté de Joigny », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1882, 36 (prieuré de Joigny).
Chapotin M.-D., Histoire des Dominicains de la province de France, Rouen, 1898.
Chartraire C., « La Vierge de la cathédrale de Sens », Bulletin de la Commission des Travaux Historiques, 1912, p. 275-288 (autel de la Vierge).
Chartraire E., « Insignes épiscopaux et fragments de vêtements liturgiques conservés au trésor de la cathédrale de Sens », Bulletin de la Commission des Travaux Historiques et Archéologiques, 1918 (p. 57 : plaque de gant, trésor de la cathédrale).
Chartraire E., « Auxerre », Dictionnaire d’Histoire et de Géographie Ecclésiastiques, t. V, Paris, 1931 (abbesse Estachie, col. 953-95?).
Chauveau E., « Procès-verbal de la séance tenue à Sens », dans Séances générales tenues à Laon en 1851 par la Société française pour la conservation des Monuments historiques (Paris, 1852) (Perceneige).
Chaveau (abbé), « Origine de la Métropole de Sens », Congrès Archéologique, Sens, 1847.
Cotteau G., Petit V., « Guide pittoresque dans le département de l’Yonne », Annuaire historique du département de l’Yonne, recueil de documents authentiques, destinés à former la statistique départementale, 23e année (1859), 24e année (1860).
Courajod L., « Le Trésor de la cathédrale d’Auxerre en 1567 », Revue archéologique, t. XIX, 1869 (janv-juin). 
De Vaivre J.-B., « Dessins de tombes médiévales bourguignonnes de la collection Gaignières », Gazette des Beaux-arts, nov. 1986.
Duruc L.-M., Bibliothèque historique de l’Yonne, t. I, Auxerre-Paris, 1890.
Frollow A., « Quelques inscriptions sur des œuvres d’art du Moyen Âge », Cahiers archéologiques, t. VI, 1952, p. 163.
Gatien-Arnoult M., « Histoire de l’université de Toulouse », Mémoires Académie des Sciences, inscriptions et belles-lettres Toulouse, 8e S. I, 1879 (p. 20, note 7 : épitaphe de Pierre de Belle-Perche).
Gaignières (coll.), Recueil de tombeaux provenant de la collection Roger de Gaignières, t. V, Champagne, Brie - Paris, BnF, Département des Estampes et de la photographie - RESERVE PE-6-FOL.
Gauthier G., « Rogny et Saint-Eusoge (Yonne) depuis les origines jusqu’à nos jours », Bulletin de la Société des sciences historiques et naturelles de l’Yonne 1986, 50.
Greenhill F. A., Incised Effigial Slab. A Study of Engraved Stone Memorials in Latin Christendom, c. 1100 to c. 1700, t. I-II, Londres, 1976.
Guilhermy F. de, Inscriptions de la France du Ve au XVIIIe siècle, t. III, Ancien diocèse de Paris, Paris, 1877 (p. 25 : Jean Jouvente).
Henrion F., « Remplois de sarcophages du haut Moyen Âge et souvenir de leur image à l’époque romane en Bourgogne et alentours », Les cahiers de Saint-Michel-de-Cuxa, 2011, XLII, p. 93-102 (Saint-Bris-le-Vineux).
Henry (abbé), Histoire de Seignelay, I, Paris-Amiens, 1989 (1833).
Julliot G., « Une inscription à Joigny », Bulletin du Comité de la Langue, de l’histoire et des arts de la France, 1857, t. IV (p. 187 : Saint-André de Joigny).
Julliot G., « L’Horloge de Sens », Bulletin de la Société archéologique de Sens, 1867, t. IX (p. 393 : cloche, horloge)
Julliot G., « Gilles de Poissy, seigneur de Ternantes et de Montchavan, son testament et sa sépulture », Bulletin de la Société archéologique de Sens, 1888, t. XIV, p. 401-403.
Julliot G., « Epitaphes des archevêques de Sens inhumés dans le sanctuaire et le chœur de la cathédrale et autres inscriptions rencontrées pendant les travaux exécutés en 1887-88 », Bulletin de la Société archéologique de Sens, 1894, t. XVI, p. 190-192 (Adémar Robert).
Lebeuf (abbé), Mémoires concernant l’histoire civile et ecclésiastique d’Auxerre et de son diocèse, t. I, Paris, 1743 ; réimp. Marseille, 1978.
Le Gendre A., « Les autels du chœur de l’église métropolitaine de Sens au Moyen Âge », Hortus artium medievalium, 2005, 11, p. 233-244 (cathédrale de Sens).
Le Maistre, « Marguerite de Bourgogne, reine de Naples, de Sicile et de Jérusalem, comtesse de Tonnerre », Annuaire historique du département de l’Yonne, 1867, 31, p. 89-90.
Le Roux de Lincy, Le livre des proverbes français, t. II, p. 175 (Lucy-le-Bois).
Lesire A., « Notes et documents pour servir à l’histoire de Toucy », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1908, vol. 62, p. 98 (Saint-Fargeau, p. 81 Toucy).
« Les sires de Noyers », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1874, vol. 28 (Noyers).
Les trésors des églises de France, Paris, 1965 (p. 437-438, tapisserie de l’Adoration des Mages).
Leviste J., « La terre et seigneurie de Mauny-le-Repos à Bagneux (corrections et compléments). Les pierres tombales de l’abbaye de Vauluisant », Bulletin de la Société Archéologie que Sens, 2000, t. 2, p. 57-82 (complément bibliographie pour la série d’inscriptions de l’abbaye de Vauluisant, CIFM 21).
Louis R., Autessiodorensum christianum. Les églises d’Auxerre des origines au XIe siècle, Paris, 1952.
Marilier J. (chan.) et Roumailhac J., « Mille ans d’épigraphie dans les cryptes de Saint-Germain d’Auxerre », Bulletin de la Société des fouilles archéologiques et des monuments historiques de l’Yonne, n° 6, 1989.
Martène E. (dom) et Durand U. (dom), Voyage littéraire de deux religieux bénédictins de la congrégation de Saint-Maur…, 1ère partie, Paris, 1717.
Martin A., « Nouvelles observations sur le portail Saint-Jean de la cathédrale de Sens », Bulletin monumental, 2005, 163-4, p. 315-327.
Molard Fr., Bonneau M. et Monceaux M., Inventaire du trésor de la cathédrale d’Auxerre, Auxerre, 1892.
Moreau A., Eglises de l’Yonne, Paris, s.d.
Morellet N. J., Barat J.-C. et Bussière E., Le Nivernois, album historique et pittoresque, rééd. Grenoble, 1969, t. II, p. 175 (Saint-Père).
Parat (abbé), « Etude rurale, Bois d’Arcy et son prieuré », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1906, vol. 60, p. 40-41 (prieuré Sainte-Radegonde).
Parat (abbé), « Note sur une inscription de Lucy-le-Bois près d’Avallon », Bulletin de la Commission des Travaux Historiques, 1918, p. LXXXIV.
Parat (abbé), « L’abbaye de Marcilly », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1925, 79 (dont Provency).
Peindre à Auxerre au Moyen Âge, IXe-XIVe siècles : 10 ans de recherche à l’abbaye Saint-Germain et à la cathédrale Saint-Etienne d’Auxerre, sous dir. Sapin Ch., Auxerre, Centre d’études médiévales, Pars, éd. CTHS, 1999.
Petit E., « Les Bourguignons de l’Yonne à la cour de Philippe de Valois », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1898, 52.
Pissier (abbé A.), « Notice historique sur Saint-Père-sous-Vézelay », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1903, vol. 56, p. 300-301.
Porée Ch., « Epigraphie campanaire de l’Yonne, suivie d’une liste de fondeurs ayant travaillé dans la région », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1916, vol. 70.
Potthast A., Bibliotheca medii aevi, t. I, Berlin, 1896.
Prou M., « Inscriptions carolingiennes des cryptes de Saint-Germain d’Auxerre », Gazette archéologique, t. XIII, 1888, p. 300-302.
Quantin M., Répertoire archéologique du département de l’Yonne, Paris, 1868 (col. 255, Neuvy-Sautour, cuve baptismale).
Quesvers P. et Stein H., Inscriptions de l’ancien diocèse de Sens, t. III, Paris, 1902.
Rivières (baron de), « Inscriptions et devises horaires », Bulletin monumental, 1884, t. L (p. 443-444 : cloche de Saint-Etienne).
Sapin Ch., « Architecture et décor à Saint-Germain d’Auxerre au IXe siècle : un ou des programmes adaptés », Le programme. Une notion pertinente en histoire de l’art médiéval ?, études réunies par J.-M. Guillouët et C. Rabel, Paris, 2011, p. 57-78.
Saulnier L. et Stratford N., La sculpture oubliée de Vézelay, Paris, 1984.
Sauvageot C., « Etude sur les cloches », Annales Archéologiques, 1862, XXII, p. 226-227 (cloche de l’église Saint-Jean de Joigny).
Smyttere P. J. E. de, « Recherches historiques sur la Puisaye, Saint-Fargeau, Toucy (XIIIe, XIVe et XVe s.) », Bulletin de la Société des sciences historiques et naturelles de l’Yonne, 1869, vol. 23, p. 50 (Saint-Fargeau).
Tarbé T., Recherches historiques et anecdotiques sur la ville de Sens, sur son antiquité et ses monuments, Paris, 2e éd., 1988 (Saint-Sauveur des Vignes).
Villetard (abbé), « La prière « Avete, omnes animere » au petit portail de Saint-Lazare d’Avallon », Congrès archéologique de France, Avallon, 1907 (Paris-Caen, 1908).
Viollet-le-Duc, Dictionnaire raisonné du mobilier français, t. I, Paris, 1871.

